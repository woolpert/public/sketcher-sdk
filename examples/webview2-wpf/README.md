# Sketcher WebView2 WPF Example

This example demonstrates how to intergrate a WPF .NET application with the Microsoft Edge WebView2 control and Sketcher. Learn more about WebView2 at: https://docs.microsoft.com/en-us/microsoft-edge/webview2/

## Setup

### Prerequisites
* Visual Studio 16+

### Execute

Run WebView2Demo from the Visual Studio launch menu.