﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WebView2Demo.Models
{
    public class Sample
    {
        public string Name { get; set; }
        public string Content { get; set; }
    }
}
