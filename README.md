# Sketcher SDK

Sketcher is a hosted web application created specifically for computer assisted mass appraisal (CAMA).

This SDK provides guides and examples for how to integrate the Sketcher with your CAMA software.

## Licensing

Sketcher requires a CAMA Cloud License to access. To request a trial key please [contact us](https://www.datacloudsolutions.net/contact.php) or follow the instructions to recover your license credentials [here](https://auth.camacloud.com/info/forgot.aspx).

## Integration

Refer to the [integration guide](./docs/integration-guide.md) for detailed information about how to integrate your application with the Sketcher.

## Integration Implementations

- [Web Browser](./examples/browser)
- [WebView2 for WPF](./examples/webview2-wpf)
